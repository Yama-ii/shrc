export EDITOR="nvim"
export BROWSER="vieb"

alias scst="systemctl start"
alias scsp="systemctl stop"
alias scss="systemctl status"

alias get="paru -S --noconfirm"
alias upd="paru -Syu --noconfirm"
alias del="paru -Rnsu --noconfirm"
alias ser="paru -Ss"

alias hwd="howdoi --color"

alias cpup="sudo cpupower frequency-set -g performance"
alias cpnorm="sudo cpupower frequency-set -g schedutil"

alias erc="$EDITOR ~/.config/fish/config.fish"
alias src="source ~/.config/fish/config.fish"

alias C="clear"
alias n="nvim"
alias Q="exit"

alias tn="tmux new -s"
alias ta="tmux attach -t"
alias tl="tmux ls"

alias tran="trans -show-languages n -show-original-phonetics n -show-translation-phonetics n -show-dictionary n -show-prompt-message n"
alias btran="trans -brief"
alias btr="trans -brief :ru "

alias pdf="mupdf"

setxkbmap -model pc105 -layout us,ru -option grp:alt_shift_toggle